import React, { useState, useEffect } from 'react';
//import { v4 as uuid } from 'uuid';
//import localStorage from 'local-storage';
import './styles/App.css';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
//import DetalleTarea from './components/DetalleTarea.js';
import InputBuscador from './components/InputBuscador';
import MensajeNoEncontrado from './components/MensajeNoEncontrado';
import PendingTaskCounter from './components/PendingTaskCounter';
import CompletedTaskCounter from './components/CompletedTaskCounter';
import axios from 'axios';

function App() {
	const [resultadosEncontrados, setResultadosEncontrados] = useState(true);

	const [tasks, setTasks] = useState([]);
	const [newTask, setNewTask] = useState({
		_id: null,
		title: '',
		description: '',
		dueDate: null,
		priority: 'Normal',
		estado: 'pendiente', // Establece el estado como "pendiente" al agregar una nueva tarea
	});
	const [searchKeyword, setSearchKeyword] = useState('');
	//const [mostrarModal, setMostrarModal] = useState(false);
	//const [tareaSeleccionada, setTareaSeleccionada] = useState(null);
	const [pendingTaskCount, setPendingTaskCount] = useState(0);
	const [completedTaskCount] = useState(0); // Contador de tareas terminadas

	useEffect(() => {
		const fetchData = async () => {
			try {
				const response = await axios.get('http://localhost:3000/api/note');
				const tasksFromAPI = response.data;
				setTasks(tasksFromAPI);

				// Calcula el contador de tareas pendientes
				const pendingTasks = tasksFromAPI.filter(
					(task) => task.estado === 'pendiente'
				);
				setPendingTaskCount(pendingTasks.length);
			} catch (error) {
				console.error('Error al obtener las tareas desde la API:', error);
			}
		};

		fetchData();
	}, [pendingTaskCount]);

	const agregarTarea = async (e) => {
		e.preventDefault();

		if (newTask.title.trim() === '') return;

		try {
			const response = await axios.post(
				'http://localhost:3000/api/note',
				newTask
			);

			const savedTask = response.data;

			setTasks((prevTasks) => [...prevTasks, savedTask]);
			setNewTask({
				_id: null,
				title: '',
				description: '',
				dueDate: null,
				priority: 'Normal',
				estado: 'pendiente',
			});
		} catch (error) {
			console.error(error);
			// Manejar el error, posiblemente mostrar un mensaje al usuario
		}
	};

	const eliminarTarea = async (taskId) => {
		if (!taskId || typeof taskId !== 'string') {
			console.error('ID de tarea no válido:', taskId);
			return;
		}

		try {
			await axios.delete(`http://localhost:3000/api/note/${taskId}`);
			// Calcula el nuevo contador de tareas pendientes después de eliminar
			setTasks((prevTasks) => {
				const updatedTasks = prevTasks.filter(
					(task) => task._id !== taskId
				);
				const pendingTasks = updatedTasks.filter(
					(task) => task.estado === 'pendiente'
				);
				setPendingTaskCount(pendingTasks.length);
				return updatedTasks;
			});
		} catch (error) {
			console.error('Error al eliminar la tarea:(', error);
			// Manejar el error, posiblemente mostrar un mensaje al usuario
			return;
		}
	};

	const buscarTareas = (keyword) => {
		setSearchKeyword(keyword);

		const resultados = tasks.filter((task) =>
			task.title.toLowerCase().includes(keyword.toLowerCase())
		);

		setResultadosEncontrados(resultados.length > 0);
	};

	// const mostrarDetalleTarea = (tarea) => {
	// 	setTareaSeleccionada(tarea);
	// 	setMostrarModal(true);
	// };

	// const cambiarEstado = () => {
	// 	if (tareaSeleccionada) {
	// 		const updatedTasks = tasks.map((task) =>
	// 			task._id === tareaSeleccionada.id
	// 				? {
	// 						...task,
	// 						estado:
	// 							task.estado === 'pendiente' ? 'terminado' : 'pendiente',
	// 				  }
	// 				: task
	// 		);
	// 		setTasks(updatedTasks);
	// 		setTareaSeleccionada({
	// 			...tareaSeleccionada,
	// 			estado:
	// 				tareaSeleccionada.estado === 'pendiente'
	// 					? 'terminado'
	// 					: 'pendiente',
	// 		});
	// 	}
	// };

	// const cerrarModal = () => {
	// 	setTareaSeleccionada(null);
	// 	setMostrarModal(false);
	// };

	return (
		<div className='ContainerPrincipal'>
			<h1 style={{ textAlign: 'center', color:'#22333b' }}>APP Tareas</h1>


			<InputBuscador
				value={searchKeyword}
				onChange={(value) => buscarTareas(value)}
			/>



			<form onSubmit={agregarTarea} className='CuadroAgregar'>
				<input
					className='inputTitutoTarea'
					type='text'
					placeholder='Título'
					value={newTask.title}
					style={{ textAlign: 'center', color:'#22333b', borderRadius:60, height: 30, width: 300}}
					onChange={(e) =>
						setNewTask({ ...newTask, title: e.target.value })
					}
				/>
				<textarea
					className='inputDescTarea'
					type='text'
					placeholder='Descripción'
					value={newTask.description}
					style={{ textAlign: 'center', color:'#22333b', borderRadius:30, height: 150, width: 700}}
					onChange={(e) =>
						setNewTask({ ...newTask, description: e.target.value })
					}
				/>
				<div
					style={{
						display: 'flex',
						flexDirection: 'row',
						alignItems: 'flex-start',
						justifyContent: 'space-between',
					}}
				>
					<div
						style={{
							display: 'flex',
							flexDirection: 'column',
							marginRight: '40px',
						}}
						className='datePickerContainer'
					>
						<label className='datePickerLabel'>
							Fecha de vencimiento:
						</label>
						<DatePicker
							selected={newTask.dueDate}
							onChange={(date) =>
								setNewTask({ ...newTask, dueDate: date })
							}
							showTimeSelect
							timeFormat='HH:mm'
							timeIntervals={15}
							timeCaption='Hora'
							dateFormat='MMMM d, yyyy h:mm aa'
							placeholderText='DD/MM/AAAA 00:00:00'
						/>
					</div>
					<div
						style={{
							display: 'flex',
							flexDirection: 'column',
							marginRight: '40px',
						}}
					>
						<label className='datePickerLabel'>Prioridad:</label>
						<select
							className='inputTituloTarea'
							value={newTask.priority}
							onChange={(e) =>
								setNewTask({ ...newTask, priority: e.target.value })
							}
						>
							<option value='Urgente'>Urgente</option>
							<option value='Normal'>Normal</option>
							<option value='Poco Urgente'>Poco Urgente</option>
						</select>
					</div>
					
				</div>
				<button
						className='btnAgregarTarea'
						type='submit'
						style={{ marginTop: '15px', height: 50, width: 200}}
					>
						Agregar Tarea
					</button>

					
<PendingTaskCounter count={pendingTaskCount} />

<CompletedTaskCounter count={completedTaskCount} />

			</form>

			{resultadosEncontrados ? (
				<ul className='ulListaTareas'>
					{tasks
						.filter((task) =>
							task.title
								.toLowerCase()
								.includes(searchKeyword.toLowerCase())
						)
						.map((task) => (
							<li
								className={`tareaListada ${
									task.estado === 'pendiente'
										? 'pendiente'
										: 'terminado'
								}`}
								key={task._id}
								//onClick={() => mostrarDetalleTarea(task)}
							>
								<strong>Título:</strong> {task.title} <br />
								<strong>Descripción:</strong> {task.description} <br />
								<button
									className='btnEliminarTarea'
									onClick={() => eliminarTarea(task._id)}
								>
									Eliminar
								</button>
							</li>
						))}
				</ul>
			) : (
				<MensajeNoEncontrado />
			)}

			{/* {mostrarModal && (
				<DetalleTarea
					tarea={tareaSeleccionada}
					onUpdateEstado={cambiarEstado}
					onClose={cerrarModal}
				/>
			)} */}
		</div>
	);
}

export default App;
