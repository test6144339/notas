//sw-utils.js
// Guardar en el cache dinamico
function actualizaCacheDinamico(cacheDinamico, req, res) {
	if (res.ok) {
		return caches.open(cacheDinamico).then((cache) => {
			cache.put(req, res.clone());

			return res.clone();
		});
	} else {
		return res;
	}
}
// Cache with network update
function actualizaCacheStatico(estaticoCache, req, APP_SHELL_INMUTABLE) {
	if (APP_SHELL_INMUTABLE.includes(req.url)) {
		// No hace falta actualizar el inmutable
		// console.log('existe en inmutable', req.url );
	} else {
		// console.log('actualizando', req.url );
		return fetch(req).then((res) => {
			return actualizaCacheDinamico(estaticoCache, req, res);
		});
	}
}

//Estrategia de cache Red y actualización de cache

function manejoApi(cacheName, req) {
	return fetch(req)
		.then((res) => {
			if (res.ok) {
				actualizaCacheDinamico(cacheName, req, res.clone());
				return res.clone();
			} else {
				return caches.match(req);
			}
		})
		.catch((err) => {
			return caches.match(req);
		});
}
