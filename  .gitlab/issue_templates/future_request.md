## Descripción de la Solicitud de Funcionalidad Futura

Describe la funcionalidad que se solicita para el futuro en este issue.

## Detalles de la Funcionalidad

Detalla la funcionalidad que se desea agregar en el futuro.

## Razón de la Solicitud

Explica por qué esta funcionalidad es importante o beneficiosa.

## Otros Comentarios

Añade cualquier comentario adicional que pueda ser relevante para esta solicitud de funcionalidad futura.
